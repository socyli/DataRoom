<div align="center">
  <h1>DataRoom</h1>
  <strong>简单、免费的开源大屏设计器</strong>
</div>

<p align="center">
    <img alt="stars" src="https://gitee.com/gcpaas/DataRoom/badge/star.svg?theme=dark">
    <img alt="forks" src="https://gitee.com/gcpaas/DataRoom/badge/fork.svg?theme=dark">
    <img alt="GitHub license" src="https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg">
    <img alt="npm" src="https://img.shields.io/npm/v/@gcpaas/data-room-ui">
    <img alt="Maven Central" src="https://img.shields.io/maven-central/v/com.gccloud/dataroom-core">
    <img alt="Company" src="https://img.shields.io/badge/Author-科大国创云网科技有限公司-blue.svg">
</p>

🔥DataRoom是一款基于SpringBoot、MyBatisPlus、ElementUI、G2Plot、Echarts等技术栈的大屏设计器，具备大屏设计、预览能力，支持MySQL、Oracle、PostgreSQL、MSSQL、JSON、JS、HTTP、Groovy等数据集接入，使用简单，完全免费，代码开源。

## 效果图

### 1. 大屏管理

进行大屏【新增】、【编辑】、【设计】、【预览】、【复制】、【删除】操作

<br>

<img  src="./doc/images/大屏管理.png" height="200" >

### 2. 设计器

采用拖拉拽可视化设计，支持20+种图表组件、15种边框组件、10多种修饰组件

<br>

<img  src="./doc/images/设计器.png" height="200" >

### 3. 资源库

支持资源自定义上传、在大屏设计器中直接引用资源，如：3D图片、边框图片、装饰条、背景图

<br>

<img alt="logo" src="./doc/images/资源库.png"   height="200">


### 4. 数据源

支持MySQL、PostgreSQL、Oracle 、ClickHouse数据库接入

<br>

<img alt="logo" src="./doc/images/数据源.png" height="200">

### 4. 数据集

支持原始数据集、自助数据集、存储过程数据集、JSON数据集、脚本数据集、JS脚本数据集、HTTP数据集多种方式接入数据

<br>

<img alt="logo" src="./doc/images/数据集.png"   height="200">
<br>
<img alt="logo" src="./doc/images/数据集2.png"   height="200">


## 优势

✅ 一站式大屏解决方案，从<span style='color:red'>**数据接入**</span>-><span style='color:red'>**大屏设计**</span>-><span style='color:red'>**大屏预览**</span>-><span style='color:red'>**生产使用**</span><br/>
✅ 支持<span style='color:red'>**多种数据集接入**</span>，满足大多数数据接入需求<br/>
✅ 支持大屏🔥<span style='color:red'>**独立部署**</span>，不对原有工程产生影响，适用于老项目<br/>
✅ 支持大屏🔥<span style='color:red'>**嵌入式集成**</span>，与项目无缝融合，引入依赖包即可，无其他系统框架依赖，减少运维成本，适用于新项目<br/>
✅ 支持🔥<span style='color:red'>**组件在线、离线开发**</span>，在线开发简单组件、离线开发复杂组件<br/>
✅ 支持自定义接口权限、数据权限，轻松对接🔥<span style='color:red'>**Shiro、Security**</span>等认证框架，保证大屏数据安全<br/>

<br/>

## 特性

| 特性                                                         | 支持 |
| :----------------------------------------------------------- | ---- |
| 支持大屏、设计、预览、导出图片、项目集成                     | ✅          |
| 支持图层上下调整，支持置于顶层、置于底层                     | ✅    |
| 支持画布组件框选、组合、取消组合、锁定、批量删除、复制功能   | ✅    |
| 支持文本、图片、轮播表、排名表、翻牌器、基础表格、倒计时、系统时间、外链基础组件 | ✅    |
| 支持折线图、梯形图、柱状图、面积图、条形图、饼图、环图、水波图、仪表盘、进度条、词云图、雷达图、漏斗图等图表组件 | ✅    |
| 支持15种边框组件，具备动画、渐变色设置                       | ✅    |
| 支持10多种修饰组件，具备动画、渐变色设置                     | ✅    |
| 支持资源管理，包含LOGO、3D图标、2D图标、修饰条、背景图等上百个大屏设计资源，资源支持自定义上传 | ✅    |
| 支持组件管理，包含系统组件、自定义组件、业务组件，组件支持二次开发 | ✅    |
| 支持多种数据源，目前支持MySQL、PostgreSQL、Oracle、ClickHouse数据库 | ✅    |
| 支持多种数据集，目前支持原始数据集、自助数据集、存储过程数据集、JSON数据集、JS数据集、脚本数据集、HTTP数据集，接入不同来源数据 | ✅    |
| 支持自定义权限、具备自定义接口权限、数据权限，保证大屏数据安全 | ✅    |
| 支持组件自定义规范，按照规范开发自己的大屏组件，满足特殊需求，如：接入three.js | ✅    |

<br/>

## 快速开始

> 如果你想源码启动，[👉 请点击这里](https://www.yuque.com/chuinixiongkou/bigscreen/ofy1bqhqgua1fu0f)

> 如果你想使用Docker启动 ，[👉 请点击这里](https://www.yuque.com/chuinixiongkou/bigscreen/ahhq3i7zxea46ox2)

> 如果你想将大屏集成到项目中，[👉 请点击这里](https://www.yuque.com/chuinixiongkou/bigscreen/ofy1bqhqgua1fu0f)

> 如果你想设计一个大屏，[👉 请点击这里](https://www.yuque.com/chuinixiongkou/bigscreen/ofy1bqhqgua1fu0f)

<br/>

## 演示环境

演示环境会不定期进行更新，请不要在演示环境中放入生产数据 <a href="http://gcpaas.gccloud.com/bigScreen" target="_blank">立即在线使用</a>

<br/>

## 常见问题

* [使用手册、二次开发、部署手册、常见问题](https://www.yuque.com/chuinixiongkou/bigscreen/index)
* [代码仓库(GitHub)](https://github.com/gcpaas/DataRoom)、[代码仓库(码云)](https://gitee.com/gcpaas/DataRoom)

<br/>

## 生态插件

| 组件         | 地址                                                         |
| ------------ | ------------------------------------------------------------ |
| 大屏设计器   | [码云](https://gitee.com/gcpaas/DataRoom)、[GitHub](https://github.com/gcpaas/DataRoom) |
| 仪表盘设计器 | [码云](https://gitee.com/gcpaas/DashBoard)、[GitHub](https://github.com/gcpaas/DashBoard) |
| 数据集       | [码云](https://gitee.com/gcpaas/dataset)、[GitHub](https://github.com/gcpaas/dataset) |

<br/>


## 联系我们

<p>
    <img alt="logo" width="200" src="./doc/images/qq.png">
</p>


## 许可证

Apache License 2.0